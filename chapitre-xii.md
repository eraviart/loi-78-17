<!-- TITLE: Chapitre XII -->
<!-- SUBTITLE: Transferts de données à caractère personnel vers des Etats n'appartenant pas à la Communauté européenne. -->

* [Article 68](/loi-2018/chapitre-xii/article-68)
* [Article 69](/loi-2018/chapitre-xii/article-69)
* [Article 70](/loi-2018/chapitre-xii/article-70)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 4 — TRANSFERTS INTERNATIONAUX](/etude-impact-490/titre-iii/article-19-section-4)

<!-- FIN REFERENCES -->

