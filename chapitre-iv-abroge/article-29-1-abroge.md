<!-- TITLE: Article 29-1 (abrogé) -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [Loi n°2000-321 du 12 avril 2000 - art. 5 JORF 13 avril 2000](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000006529189&dateTexte=20000413&categorieLien=id#LEGIARTI000006529189)
* Abrogé par [Loi n°2004-801 du 6 août 2004 - art. 4 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529397&dateTexte=20040807&categorieLien=id#LEGIARTI000006529397)

----

