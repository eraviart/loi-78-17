<!-- TITLE: Chapitre V -->
<!-- SUBTITLE: Obligations incombant aux responsables de traitements et droits des personnes -->

* [Section 1 — Obligations incombant aux responsables de traitements.](/loi-2018/chapitre-v/section-1)

  * [Article 32](/loi-2018/chapitre-v/section-1/article-32)
  * [Article 33](/loi-2018/chapitre-v/section-1/article-33)
  * [Article 34](/loi-2018/chapitre-v/section-1/article-34)
  * [Article 34 bis](/loi-2018/chapitre-v/section-1/article-34-bis)
  * [Article 35](/loi-2018/chapitre-v/section-1/article-35)
  * [Article 36](/loi-2018/chapitre-v/section-1/article-36)
  * [Article 37](/loi-2018/chapitre-v/section-1/article-37)

* [Section 2 — Droits des personnes à l'égard des traitements de données à caractère personnel.](/loi-2018/chapitre-v/section-2)

  * [Article 38](/loi-2018/chapitre-v/section-2/article-38)
  * [Article 39](/loi-2018/chapitre-v/section-2/article-39)
  * [Article 40](/loi-2018/chapitre-v/section-2/article-40)
  * [Article 40-1](/loi-2018/chapitre-v/section-2/article-40-1)
  * [Article 41](/loi-2018/chapitre-v/section-2/article-41)
  * [Article 42](/loi-2018/chapitre-v/section-2/article-42)
  * [Article 43](/loi-2018/chapitre-v/section-2/article-43)
  * [Article 43 bis](/loi-2018/chapitre-v/section-2/article-43-bis)
  * [Article 43 ter](/loi-2018/chapitre-v/section-2/article-43-ter)
  * [Article 43 quater](/loi-2018/chapitre-v/section-2/article-43-quater)
  * [Article 43 quinquies](/loi-2018/chapitre-v/section-2/article-43-quinquies)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 10 — SOUS-TRAITANT](/etude-impact-490/titre-ii/chapitre-iii/article-10)

<!-- FIN REFERENCES -->

