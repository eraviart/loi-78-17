<!-- TITLE: Signataires -->

Le président de la République : VALERY GISCARD D'ESTAING.

Le premier ministre : RAYMOND BARRE.

Le garde des sceaux, ministre de la justice : ALAIN PEYREFITTE.

Le ministre de l'intérieur : CHRISTIAN BONNET.

Le ministre de la défense : YVON BOURGES.

Le ministre délégué à l'économie et aux finances : ROBERT BOULIN.

Le ministre de l'équipement et de l'aménagement du territoire :

FERNAND ICART.

Le ministre de l'éducation : RENE HABY.

Le ministre de l'industrie, du commerce et de l'artisanat :

RENE MONORY.

Le ministre du travail : CHRISTIAN BEULLAC.

Le ministre de la santé et de la sécurité sociale : SIMONE VEIL.
