<!-- TITLE: Chapitre VII -->
<!-- SUBTITLE: Mesures et sanctions prises par la formation restreinte de la Commission nationale de l’informatique et des libertés -->

* [Article 45](/loi-2018/chapitre-vii/article-45)
* [Article 46](/loi-2018/chapitre-vii/article-46)
* [Article 47](/loi-2018/chapitre-vii/article-47)
* [Article 48](/loi-2018/chapitre-vii/article-48)
* [Article 49](/loi-2018/chapitre-vii/article-49)
* [Article 49-1](/loi-2018/chapitre-vii/article-49-1)
* [Article 49-2](/loi-2018/chapitre-vii/article-49-2)
* [Article 49-3](/loi-2018/chapitre-vii/article-49-3)
* [Article 49-4](/loi-2018/chapitre-vii/article-49-4)
* [Article 49 bis](/loi-2018/chapitre-vii/article-49-bis)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 3](/pjl-490/titre-i/chapitre-i/article-3)
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)

<!-- FIN REFERENCES -->

