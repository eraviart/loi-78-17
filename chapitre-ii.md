<!-- TITLE: Chapitre II -->
<!-- SUBTITLE: Conditions de licéité des traitements de données à caractère personnel -->

* [Section 1 — Dispositions générales](/loi-2018/chapitre-ii/section-1)

  * [Article 6](/loi-2018/chapitre-ii/section-1/article-6)
  * [Article 7](/loi-2018/chapitre-ii/section-1/article-7)

* [Section 2 — Dispositions propres à certaines catégories de données](/loi-2018/chapitre-ii/section-2)

  * [Article 8](/loi-2018/chapitre-ii/section-2/article-8)
  * [Article 9](/loi-2018/chapitre-ii/section-2/article-9)
  * [Article 10](/loi-2018/chapitre-ii/section-2/article-10)

