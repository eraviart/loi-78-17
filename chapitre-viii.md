<!-- TITLE: Chapitre VIII -->
<!-- SUBTITLE: Dispositions pénales. -->

* [Article 50](/loi-2018/chapitre-viii/article-50)
* [Article 51](/loi-2018/chapitre-viii/article-51)
* [Article 52](/loi-2018/chapitre-viii/article-52)

