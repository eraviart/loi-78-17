<!-- TITLE: Article 44 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2014-344
 du 17 mars 2014 - art. 105](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000028738036&idArticle=LEGIARTI000028740052&dateTexte=20140318&categorieLien=id#LEGIARTI000028740052)

----

I.-Les membres de la Commission nationale de l'informatique et des libertés ainsi que les agents de ses services habilités dans les conditions définies au dernier alinéa de l'article 19 ont accès, de 6 heures à 21 heures, pour l'exercice de leurs missions, aux lieux, locaux, enceintes, installations ou établissements servant à la mise en oeuvre d'un traitement de données à caractère personnel, à l'exclusion des parties de ceux-ci affectées au domicile privé.

Le procureur de la République territorialement compétent en est préalablement informé.

II. - Le responsable de ces lieux, locaux, enceintes, installations ou établissements est informé de son droit d'opposition à la visite. Lorsqu'il exerce ce droit, la visite ne peut se dérouler qu'après l'autorisation du juge des libertés et de la détention du tribunal de grande instance dans le ressort duquel sont situés les locaux à visiter, qui statue dans des conditions fixées par décret en Conseil d'Etat. Toutefois, lorsque l'urgence, la gravité des faits à l'origine du contrôle ou le risque de destruction ou de dissimulation de documents le justifie, la visite peut avoir lieu sans que le responsable des locaux en ait été informé, sur autorisation préalable du juge des libertés et de la détention. Dans ce cas, le responsable des lieux ne peut s'opposer à la visite dont la finalité est l’exercice effectif des missions prévues au III.

La visite s'effectue sous l'autorité et le contrôle du juge des libertés et de la détention qui l'a autorisée, en présence de l'occupant des lieux ou de son représentant qui peut se faire assister d'un conseil de son choix ou, à défaut, en présence de deux témoins qui ne sont pas placés sous l'autorité des personnes chargées de procéder au contrôle.

L'ordonnance ayant autorisé la visite est exécutoire au seul vu de la minute. Elle mentionne que le juge ayant autorisé la visite peut être saisi à tout moment d'une demande de suspension ou d'arrêt de cette visite. Elle indique le délai et la voie de recours. Elle peut faire l'objet, suivant les règles prévues par le code de procédure civile, d'un appel devant le premier président de la cour d'appel. Celui-ci connaît également des recours contre le déroulement des opérations de visite.

III.-Pour l’exercice des missions confiées à la Commission nationale de l’informatique et des libertés par le règlement (UE) 2016/679 et par la présente loi, les membres et agents mentionnés au premier alinéa du I peuvent demander communication de tous documents, quel qu’en soit le support, et en prendre copie. Ils peuvent recueillir, notamment sur place ou sur convocation, tout renseignement et toute justification utiles. Ils peuvent accéder, dans des conditions préservant la confidentialité à l’égard des tiers, aux programmes informatiques et aux données, ainsi qu’en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle. Le secret ne peut leur être opposé sauf concernant les informations couvertes par le secret professionnel applicable aux relations entre un avocat et son client, par le secret des sources des traitements journalistiques ou, sous réserve des dispositions de l’alinéa suivant, par le secret médical.

Le secret médical est opposable s’agissant des informations qui figurent dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l’administration de soins ou de traitements, ou de la gestion de service de santé. Toutefois la communication des données médicales individuelles incluses dans cette catégorie de traitement peut être faite sous l’autorité et en présence d’un médecin.

En dehors des contrôles sur place et sur convocation, ils peuvent procéder à toute constatation utile ; ils peuvent notamment, à partir d'un service de communication au public en ligne, consulter les données librement accessibles ou rendues accessibles, y compris par imprudence, par négligence ou par le fait d'un tiers, le cas échéant en accédant et en se maintenant dans des systèmes de traitement automatisé de données le temps nécessaire aux constatations ; ils peuvent retranscrire les données par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle.

Il est dressé procès-verbal des vérifications et visites menées en application du présent article. Ce procès-verbal est dressé contradictoirement lorsque les vérifications et visites sont effectuées sur place ou sur convocation.

Pour le contrôle de services de communication au public en ligne, les membres et agents mentionnés au premier alinéa du I peuvent réaliser toute opération nécessaire à leur mission sous une identité d’emprunt. L’utilisation d’une identité d’emprunt est sans incidence sur la régularité des constatations effectuées conformément à l’alinéa précédent. Un décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés précise les conditions dans lesquelles ils procèdent dans ces cas à leurs constatations.

IV.-Pour les traitements intéressant la sûreté de l'Etat et qui sont dispensés de la publication de l'acte réglementaire qui les autorise en application du III de l'article 26, le décret en Conseil d'Etat qui prévoit cette dispense peut également prévoir que le traitement n'est pas soumis aux dispositions du présent article.

V.–Dans l’exercice de son pouvoir de contrôle portant sur les traitements relevant du règlement (UE) 2016/679 et de la présente loi, la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l’exercice de leur fonction juridictionnelle, par les juridictions.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [14.](/avis-conseil-etat-393836/controles/14)
* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 4 (Pouvoirs de contrôle de la Commission)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-4-pouvoirs-de-controle-de-la-commission)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 4 — POUVOIRS DE CONTRÔLE DE LA CNIL](/etude-impact-490/titre-ier/chapitre-ier/article-4)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 4](/pjl-490/titre-i/chapitre-i/article-4)

<!-- FIN REFERENCES -->

