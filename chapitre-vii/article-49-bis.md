<!-- TITLE: Article 49 bis -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [LOI n°2016-1321 du 7 octobre 2016 - art. 66](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205214&dateTexte=20161008&categorieLien=id#LEGIARTI000033205214)

----

La Commission nationale de l'informatique et des libertés peut, à la demande d'une autorité exerçant des compétences analogues aux siennes dans un Etat non membre de l'Union européenne, dès lors que celui-ci offre un niveau de protection adéquat des données à caractère personnel, procéder à des vérifications dans les mêmes conditions que celles prévues à l'article 44, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.

La commission est habilitée à communiquer les informations qu'elle recueille ou qu'elle détient, à leur demande, aux autorités exerçant des compétences analogues aux siennes dans des Etats non membres de l'Union européenne, dès lors que ceux-ci offrent un niveau de protection adéquat des données à caractère personnel.

Pour la mise en œuvre du présent article, la commission conclut préalablement une convention organisant ses relations avec l'autorité exerçant des compétences analogues aux siennes. Cette convention est publiée au Journal officiel.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)

<!-- FIN REFERENCES -->

