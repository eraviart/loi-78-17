<!-- TITLE: Article 48 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 8](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782616&dateTexte=20110330&categorieLien=id#LEGIARTI000023782616)

----

Lorsqu’un organisme de certification ou un organisme chargé du respect d’un code de conduite a manqué à ses obligations ou n’a pas respecté les dispositions du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut, le cas échéant après mise en demeure, saisir la formation restreinte de la Commission qui peut prononcer, dans les mêmes conditions que celles prévues aux articles 45 à 47, le retrait de l’agrément qui leur a été délivré..

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 6 (Mesures correctrices)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-6-mesures-correctrices)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)

<!-- FIN REFERENCES -->

