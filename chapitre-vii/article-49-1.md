<!-- TITLE: Article 49-1 -->

----

<mark style="background-color: #F8EDA8;">I. – La Commission nationale de l’informatique et des libertés coopère avec les autorités de contrôle des autres États membres de l’Union européenne en application de l’article 62 du règlement (UE) 2016/679, dans les conditions prévues au présent article. Cette coopération n’est pas applicable aux traitements qui ne relèvent pas du champ d’application du droit de l’Union européenne.</mark>

<mark style="background-color: #F8EDA8;">II. – Qu’elle agisse en tant qu’autorité de contrôle chef de file ou en tant qu’autorité concernée au sens des articles 4 et 56 du règlement (UE) 2016/679, la Commission nationale de l’informatique et des libertés est compétente pour traiter une réclamation ou une éventuelle violation des dispositions du même règlement affectant par ailleurs d’autres États membres. Le président de la commission invite les autres autorités de contrôle concernées à participer aux opérations de contrôle conjointes qu’il décide de conduire.</mark>

<mark style="background-color: #F8EDA8;">  III. – Lorsqu’une opération de contrôle conjointe se déroule sur le territoire français, des membres ou agents habilités de la commission, agissant en tant qu’autorité de contrôle d’accueil, sont présents aux côtés des membres et agents des autres autorités de contrôle participant, le cas échéant, à l’opération. À la demande de l’autorité de contrôle de l’État membre, le président de la commission peut habiliter, par décision particulière, ceux des membres ou agents de l’autorité de contrôle concernée qui présentent des garanties comparables à celles requises des agents de la commission, en application des dispositions de l’article 19, à exercer, sous son autorité, tout ou partie des pouvoirs de vérification et d’enquête dont disposent les membres et les agents de la commission.</mark>

<mark style="background-color: #F8EDA8;">  IV. – Lorsque la commission est invitée à contribuer à une opération de contrôle conjointe décidée par une autre autorité compétente, le président de la commission se prononce sur le principe et les conditions de la participation, désigne les membres et agents habilités, et en informe l’autorité requérante dans les conditions prévues à l’article 62 du règlement (UE) 2016/679.</mark>

<!-- DEBUT REFERENCES -->

----

<!-- FIN REFERENCES -->

