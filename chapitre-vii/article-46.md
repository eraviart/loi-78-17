<!-- TITLE: Article 46 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 64](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205210&dateTexte=20161008&categorieLien=id#LEGIARTI000033205210)

----

I. – Lorsque le non-respect des dispositions du règlement (UE) 2016/679 ou de la présente loi entraîne une violation des droits et libertés mentionnés à l’article 1er et que le président de la commission considère qu’il est urgent d’intervenir, il saisit la formation restreinte qui peut, dans le cadre d’une procédure d’urgence contradictoire définie par décret en Conseil d’État, adopter l’une des mesures suivantes :

1° L’interruption provisoire de la mise en œuvre du traitement, y compris d’un transfert de données hors de l’Union européenne, pour une durée maximale de trois mois, si le traitement n’est pas au nombre de ceux qui sont mentionnés aux I et II de l’article 26 et ceux mentionnées à l’article 27 ;

2° La limitation du traitement de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n’est pas au nombre de ceux qui sont mentionnés aux I et II de l’article 26 ;

3° La suspension provisoire de la certification délivrée au responsable du traitement ou au sous-traitant ;

4° La suspension provisoire de l’agrément délivré à un organisme de certification ou un organisme chargé du respect d’un code de conduite ;

5° La suspension provisoire de l’autorisation délivrée sur le fondement du III de l’article 54 du chapitre IX de la présente loi.

6° L’injonction de mettre en conformité le traitement avec les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l’État, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;

7° Un rappel à l’ordre ;

8° L’information du Premier ministre pour qu’il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l’article 26. Le Premier ministre fait alors connaître à la formation restreinte les suites qu’il a données à cette information au plus tard quinze jours après l’avoir reçue.

II. – Dans les circonstances exceptionnelles prévues au 1 de l’article 66 du règlement (UE) 2016/679, lorsque la formation restreinte adopte les mesures provisoires prévues aux 1° à 4° du I du présent article, elle informe sans délai de la teneur des mesures prises et de leurs motifs les autres autorités de contrôle concernées, le Comité européen de la protection des données et la Commission européenne.

Lorsque la formation restreinte a pris de telles mesures et qu’elle estime que des mesures définitives doivent être prises, elle met en œuvre les dispositions du 2 de l’article 66 du règlement.

III. – Pour les traitements régis par le chapitre XIII, lorsqu’une autorité de contrôle compétente en vertu du règlement (UE) 2016/679 n’a pas pris de mesure appropriée dans une situation où il est urgent d’intervenir afin de protéger les droits et libertés des personnes concernées, la formation restreinte, saisie par le président de la commission, peut demander au comité européen de la protection des données un avis d’urgence ou une décision contraignante d’urgence dans les conditions et selon les modalités prévues aux 3 et 4 de l’article 66 de ce règlement.

IV. – En cas d’atteinte grave et immédiate aux droits et libertés mentionnés à l’article 1er, le président de la commission peut en outre demander, par la voie du référé, à la juridiction compétente d’ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)

<!-- FIN REFERENCES -->

