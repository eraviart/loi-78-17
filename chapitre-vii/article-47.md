<!-- TITLE: Article 47 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 65 (V)](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205212&dateTexte=20161008&categorieLien=id#LEGIARTI000033205212)

----

Les mesures prévues au II de l’article 45 et aux 1° à 6° du I de l’article 46 sont prononcées sur la base d’un rapport établi par l’un des membres de la Commission nationale de l’informatique et des libertés, désigné par le président de celle-ci parmi les membres n’appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement ou au sous-traitant, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l’audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général, les agents des services.

La formation restreinte peut rendre publiques les mesures qu’elle prend. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu’elle désigne aux frais des personnes sanctionnées.

Sans préjudice des obligations d’information qui leur incombent en application de l’article 34 du règlement (UE) 2016/679, la formation restreinte peut ordonner que le responsable ou le sous-traitant concerné informe individuellement, à ses frais, chacune des personnes concernées de la violation des dispositions de la présente loi ou du règlement précité relevée ainsi que, le cas échéant, de la mesure prononcée.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)

<!-- FIN REFERENCES -->

