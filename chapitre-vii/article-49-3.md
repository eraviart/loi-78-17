<!-- TITLE: Article 49-1 -->

----

<mark style="background-color: #F8EDA8;">
Lorsque la commission agit en tant qu’autorité de contrôle chef de file s’agissant d’un traitement transfrontalier au sein de l’Union européenne, elle communique le rapport du membre rapporteur, ainsi que l’ensemble des informations utiles de la procédure ayant permis d’établir le rapport, aux autres autorités de contrôle concernées sans tarder et avant l’éventuelle audition du responsable du traitement ou du sous-traitant. Les autorités concernées sont mises en mesure d’assister à l’audition par la formation restreinte du responsable de traitement ou du sous-traitant par tout moyen de retransmission approprié, ou de prendre connaissance d’un procès-verbal dressé à la suite de l’audition. </mark>

<mark style="background-color: #F8EDA8;">Après en avoir délibéré, la formation restreinte soumet son projet de décision aux autres autorités concernées conformément à la procédure définie à l’article 60 du règlement (UE) 2016/679. À ce titre, elle se prononce sur la prise en compte des objections pertinentes et motivées émises par les autorités concernées et saisit, si elle décide d’écarter l’une des objections, le comité européen de la protection des données conformément à l’article 65 du règlement.
</mark>

<mark style="background-color: #F8EDA8;">Les conditions d’application du présent article sont définies par un décret en Conseil d’État, après avis de la Commission nationale de l’informatique et des libertés.</mark>

<!-- DEBUT REFERENCES -->

----

<!-- FIN REFERENCES -->

