<!-- TITLE: Article 45 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 64](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205210&dateTexte=20161008&categorieLien=id#LEGIARTI000033205210)

----

I. – Le président de la Commission nationale de l’informatique et des libertés peut avertir un responsable du traitement ou un sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions du règlement (UE) 2016/679 ou de la présente loi.

II. – Lorsque le responsable du traitement ou le sous-traitant ne respecte pas les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut saisir la formation restreinte de la commission en vue du prononcé, après procédure contradictoire, de l’une ou de plusieurs des mesures suivantes :

1° Un rappel à l’ordre ;

2° Une injonction de mettre en conformité le traitement avec les obligations résultant de la présente loi ou du règlement (UE) 2016/679 ou de satisfaire aux demandes présentées par la personne concernée en vue d’exercer ses droits, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l’État, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;

3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense, la limitation temporaire ou définitive du traitement, son interdiction ou le retrait d’une autorisation accordée en application du règlement (UE) 2016/679 ou de la présente loi ;

4° Le retrait d’une certification ou l’injonction, à l’organisme concerné, de refuser ou de retirer la certification accordée ;

5° La suspension des flux de données adressées à un destinataire situé dans un pays tiers ou à une organisation internationale ;

6° Le retrait de la décision d’approbation d’une règle d’entreprise contraignante ;

7° À l’exception des cas où le traitement est mis en œuvre par l’État, une amende administrative ne pouvant excéder 10 millions d’euros ou, s’agissant d’une entreprise, 2 % du chiffre d’affaires annuel mondial total de l’exercice précédent, le montant le plus élevé étant retenu. Dans les hypothèses mentionnées aux paragraphes 5 et 6 de l’article 83 du règlement (UE) 2016/679, ces plafonds sont portés respectivement à 20 millions d’euros et 4 % du chiffre d’affaires. La formation restreinte prend en compte, dans la détermination du montant de l’amende, les critères précisés à l’article 83 du règlement (UE) 2016/679.

Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que l’amende administrative s’impute sur l’amende pénale qu’il prononce.

Les sanctions pécuniaires sont recouvrées comme les créances de l’État étrangères à l’impôt et au domaine.

Le projet de mesure est le cas échéant soumis aux autres autorités concernées selon les modalités définies à l’article 60 du règlement (UE) 2016/679.

III. – Lorsque le responsable d’un traitement ou le sous-traitant ne respecte pas les obligations découlant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut également prononcer à son égard une mise en demeure, dans le délai qu’il fixe :

1° De satisfaire aux demandes présentées par la personne concernée en vue d’exercer ses droits ;

2° De mettre les opérations de traitement en conformité avec les dispositions applicables ;

3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense et ceux mentionnées à l’article 27, de communiquer à la personne concernée une violation de données à caractère personnel ;

4° De rectifier ou d’effacer des données à caractère personnel, ou de limiter le traitement.

Dans le cas prévu au 4°, le président peut, dans les mêmes conditions, mettre en demeure le responsable de traitement ou le sous-traitant de notifier aux destinataires des données les mesures qu’il a prises.

Le délai de mise en conformité peut être fixé à vingt-quatre heures en cas d’extrême urgence.

Le président prononce, le cas échéant, la clôture de la procédure de mise en demeure.

Le président peut demander au bureau de rendre publique la mise en demeure. Dans ce cas, la décision de clôture de la procédure de mise en demeure fait l’objet de la même publicité.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 6 (Mesures correctrices)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-6-mesures-correctrices)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)

<!-- FIN REFERENCES -->

