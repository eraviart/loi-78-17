<!-- TITLE: Article 49-1 -->

----

<mark style="background-color: #F8EDA8;">Lorsque la commission agit en tant qu’autorité concernée, au sens du règlement (UE) 2016/679, le président de la commission est saisi des projets de mesures correctrices soumis à la commission par une autre autorité chef de file
</mark>

<mark style="background-color: #F8EDA8;">Lorsque ces mesures sont d’objet équivalent à celles définies aux I et III de l’article 45, le président décide, le cas échéant, d’émettre une objection pertinente et motivée selon les modalités prévues à l’article 60 de ce règlement.</mark>

<mark style="background-color: #F8EDA8;">Lorsque ces mesures sont d’objet équivalent à celles définies au II de l’article 45 et à l’article 46, le président saisit la formation restreinte. Le président de la formation restreinte ou le membre de la formation restreinte qu’il désigne peut, le cas échéant, émettre une objection pertinente et motivée selon les mêmes modalités.</mark>

<!-- DEBUT REFERENCES -->

----

<!-- FIN REFERENCES -->

