<!-- TITLE: Article 49 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 8](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782616&dateTexte=20110330&categorieLien=id#LEGIARTI000023782616)

----

Dans les conditions prévues aux articles 60 à 67, du règlement (UE) 2016/679, la Commission nationale de l’informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres États membres de l’Union européenne, et réalise avec elles des opérations conjointes.

La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui les concerne, les procédures visées à l’alinéa précédent.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)

<!-- FIN REFERENCES -->

