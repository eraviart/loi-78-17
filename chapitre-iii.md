<!-- TITLE: Chapitre III -->
<!-- SUBTITLE: La Commission nationale de l'informatique et des libertés. -->

* [Article 11](/loi-2018/chapitre-iii/article-11)
* [Article 12 (abrogé)](/loi-2018/chapitre-iii/article-12-abroge)
* [Article 13](/loi-2018/chapitre-iii/article-13)
* [Article 14 (abrogé)](/loi-2018/chapitre-iii/article-14-abroge)
* [Article 15](/loi-2018/chapitre-iii/article-15)
* [Article 15 bis](/loi-2018/chapitre-iii/article-15-bis)
* [Article 16](/loi-2018/chapitre-iii/article-16)
* [Article 17](/loi-2018/chapitre-iii/article-17)
* [Article 18](/loi-2018/chapitre-iii/article-18)
* [Article 19](/loi-2018/chapitre-iii/article-19)
* [Article 20](/loi-2018/chapitre-iii/article-20)
* [Article 21](/loi-2018/chapitre-iii/article-21)

