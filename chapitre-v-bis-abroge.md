<!-- TITLE: Chapitre V bis -->
<!-- SUBTITLE: Traitements automatisés de données nominatives ayant pour fin la recherche dans le domaine de la santé. (abrogé) -->

* [Article 40-9 (abrogé)](/loi-2018/chapitre-v-bis-abroge/article-40-9-abroge)
* [Article 40-10 (abrogé)](/loi-2018/chapitre-v-bis-abroge/article-40-10-abroge)

