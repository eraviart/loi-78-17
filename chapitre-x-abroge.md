<!-- TITLE: Chapitre X -->
<!-- SUBTITLE: Traitements de données de santé à caractère personnel à des fins d'évaluation ou d'analyse des pratiques ou des activités de soins et de prévention. (abrogé) -->

* [Article 62 (abrogé)](/loi-2018/chapitre-x-abroge/article-62-abroge)
* [Article 63 (abrogé)](/loi-2018/chapitre-x-abroge/article-63-abroge)
* [Article 64 (abrogé)](/loi-2018/chapitre-x-abroge/article-64-abroge)
* [Article 65 (abrogé)](/loi-2018/chapitre-x-abroge/article-65-abroge)
* [Article 66 (abrogé)](/loi-2018/chapitre-x-abroge/article-66-abroge)

