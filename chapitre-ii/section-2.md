<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Dispositions propres à certaines catégories de données -->

* [Article 8](/loi-2018/chapitre-ii/section-2/article-8)
* [Article 9](/loi-2018/chapitre-ii/section-2/article-9)
* [Article 10](/loi-2018/chapitre-ii/section-2/article-10)

