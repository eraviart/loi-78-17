<!-- TITLE: Article 10 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 2 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529395&dateTexte=20040807&categorieLien=id#LEGIARTI000006529395)

----

Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Outre les cas mentionnés aux _a_ et _c_ sous le 2 de [l’article 22 du règlement 2016/679](/reglement-2016-679/chapitre-iii/section-4/article-22), aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée, à l’exception des décisions administratives individuelles prises dans le respect de [l’article L. 311-3-1 et du chapitre Ier du titre Ier du livre IV du code des relations du public et de l’administration](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180113), à condition que le traitement ne porte pas sur des données mentionnées au [I de l’article 8](/loi-2018/chapitre-ii/section-2/article-8).

Pour les décisions administratives mentionnées à l’alinéa précédent, le responsable du traitement s’assure de la maîtrise du traitement algorithmique et de ses évolutions.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [42.](/avis-conseil-etat-393836/decision-individuelle-automatisee/42)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 14 — DECISION ADMINISTRATIVE AUTOMATISEE](/etude-impact-490/titre-ii/chapitre-iv/article-14)
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 14](/pjl-490/titre-ii/chapitre-v/article-14)

<!-- FIN REFERENCES -->

