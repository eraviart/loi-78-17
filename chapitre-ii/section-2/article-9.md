<!-- TITLE: Article 9 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 2 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529395&dateTexte=20040807&categorieLien=id#LEGIARTI000006529395)

----

Les traitements de données à caractère personnel relatives aux condamnations pénales, aux infractions ou aux mesures de sûreté connexes ne peuvent être effectués que sous le contrôle de l’autorité publique ou par :

1° Les juridictions, les autorités publiques et les personnes morales gérant un service public, agissant dans le cadre de leurs attributions légales ainsi que les personnes morales de droit privé collaborant au service public de la justice, et appartenant à des catégories dont la liste est fixée par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés, dans la mesure strictement nécessaire à leur mission ;

2° Les auxiliaires de justice, pour les stricts besoins de l'exercice des missions qui leur sont confiées par la loi ;

3° Les personnes physiques ou morales, aux fins de leur permettre de préparer et le cas échant, d’exercer et de suivre une action en justice en tant que victime, mise en cause, ou pour le compte de ceux-ci et de faire exécuter la décision rendue, pour une durée proportionnée à cette finalité ; la communication à un tiers n’est alors possible que sous les mêmes conditions et dans la mesure strictement nécessaire à la poursuite de ces mêmes finalités ;

4° Les personnes morales mentionnées aux [articles L. 321-1 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279093&dateTexte=&categorieLien=cid)et [L. 331-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279126&dateTexte=&categorieLien=cid) du code de la propriété intellectuelle, agissant au titre des droits dont elles assurent la gestion ou pour le compte des victimes d'atteintes aux droits prévus aux livres Ier, II et III du même code aux fins d'assurer la défense de ces droits.

5 ° Les réutilisateurs des informations publiques figurant dans les jugements et décisions mentionnés aux articles L. 10 du code de justice administrative et L. 111-13 du code de l’organisation judiciaire, sous réserve que les traitements mis en œuvre n’aient ni pour objet ni pour effet de permettre la ré-identification des personnes concernées.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [27.](/avis-conseil-etat-393836/donnees-dinfraction/27)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)
  * [Article 11](/pjl-490/titre-ii/chapitre-iv/article-11)

<!-- FIN REFERENCES -->

