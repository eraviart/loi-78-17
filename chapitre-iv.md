<!-- TITLE: Chapitre IV -->
<!-- SUBTITLE: Formalités préalables à la mise en oeuvre des traitements. -->

* [Article 22](/loi-2018/chapitre-iv/article-22)

* [Section 1 — Déclaration.](/loi-2018/chapitre-iv/section-1)

  * [Article 23](/loi-2018/chapitre-iv/section-1/article-23)
  * [Article 24](/loi-2018/chapitre-iv/section-1/article-24)

* [Section 2 — Autorisation.](/loi-2018/chapitre-iv/section-2)

  * [Article 25](/loi-2018/chapitre-iv/section-2/article-25)
  * [Article 26](/loi-2018/chapitre-iv/section-2/article-26)
  * [Article 27](/loi-2018/chapitre-iv/section-2/article-27)
  * [Article 28](/loi-2018/chapitre-iv/section-2/article-28)
  * [Article 29](/loi-2018/chapitre-iv/section-2/article-29)

* [Section 3 — Dispositions communes.](/loi-2018/chapitre-iv/section-3)

  * [Article 30](/loi-2018/chapitre-iv/section-3/article-30)
  * [Article 31](/loi-2018/chapitre-iv/section-3/article-31)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)

<!-- FIN REFERENCES -->

