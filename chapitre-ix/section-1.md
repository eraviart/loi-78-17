<!-- TITLE: Section 1 -->
<!-- SUBTITLE: Dispositions générales. -->

* [Article 53](/loi-2018/chapitre-ix/section-1/article-53)
* [Article 54](/loi-2018/chapitre-ix/section-1/article-54)
* [Article 55](/loi-2018/chapitre-ix/section-1/article-55)
* [Article 56](/loi-2018/chapitre-ix/section-1/article-56)
* [Article 57](/loi-2018/chapitre-ix/section-1/article-57)
* [Article 58](/loi-2018/chapitre-ix/section-1/article-58)
* [Article 59](/loi-2018/chapitre-ix/section-1/article-59)
* [Article 60](/loi-2018/chapitre-ix/section-1/article-60)

