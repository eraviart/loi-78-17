<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Dispositions particulières aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé. -->

* [Article 61](/loi-2018/chapitre-ix/section-2/article-61)
* [Article 62](/loi-2018/chapitre-ix/section-2/article-62)
* [Article 63](/loi-2018/chapitre-ix/section-2/article-63)

