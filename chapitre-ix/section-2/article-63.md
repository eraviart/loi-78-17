<!-- TITLE: Article 63 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

L’autorisation du traitement est accordée par la Commission nationale de l’informatique et des libertés dans les conditions définies à [l’article 54](/loi-2018/chapitre-ix/article-54) et après avis :

1° Du comité compétent de protection des personnes mentionné à [l’article L. 1123-6 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685877), pour les demandes d’autorisation relatives aux recherches impliquant la personne humaine mentionnées à [l’article L. 1121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665) du même code ;

2° Du comité d’expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d’autorisation relatives à des études ou à des évaluations ainsi qu’à des recherches n’impliquant pas la personne humaine, au sens du 1° du présent article. Un décret en Conseil d’État, pris après avis de la Commission nationale de l’informatique et des libertés, fixe la composition de ce comité et définit ses règles de fonctionnement. Le comité d’expertise est soumis à [l’article L. 1451-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000022019483).

Les dossiers présentés dans le cadre de la présente section, à l’exclusion des recherches impliquant la personne humaine, sont déposés auprès d’un secrétariat unique assuré par l’Institut national des données de santé, qui assure leur orientation vers les instances compétentes.

<!-- DEBUT REFERENCES -->

----

# Références

<!-- FIN REFERENCES -->

