<!-- TITLE: Article 62 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Des méthodologies de référence sont homologuées et publiées, par la Commission nationale de l’informatique et des libertés. Elles sont établies en concertation avec l’Institut national des données de santé mentionné à [l’article L. 1462-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) et des organismes publics et privés représentatifs des acteurs concernés.

Lorsque le traitement est conforme à une méthodologie de référence, il peut être mis en œuvre, sans autorisation mentionnée à [l’article 54](/loi-2018/chapitre-ix/article-54), à la condition que son responsable adresse préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.

<!-- DEBUT REFERENCES -->

----

# Références

<!-- FIN REFERENCES -->

