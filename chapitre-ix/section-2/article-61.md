<!-- TITLE: Article 61 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Les traitements automatisés de données à caractère personnel dont la finalité est ou devient la recherche ou les études dans le domaine de la santé ainsi que l’évaluation ou l’analyse des pratiques ou des activités de soins ou de prévention sont soumis aux dispositions de la section 1 du présent chapitre, sous réserve de celles de la présente section.

<!-- DEBUT REFERENCES -->

----

# Références

<!-- FIN REFERENCES -->

