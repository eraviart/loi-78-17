<!-- TITLE: Article 53 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Outre les dispositions du [règlement (UE) 2016/679](/reglement-2016-679), les traitements contenant des données concernant la santé des personnes sont soumis aux dispositions du présent chapitre, à l’exception des catégories de traitements suivantes :

1° Les traitements relevant des 1° à 6° du II de l’[article 8](/loi-2018/chapitre-ii/section-2/article-8) ;

2° Les traitements permettant d’effectuer des études à partir des données recueillies en application du [6° du II de l’article 8](/loi-2018/chapitre-ii/section-2/article-8) lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;

3° Les traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d’un régime de base d’assurance maladie ;

4° Les traitements effectués au sein des établissements de santé par les médecins responsables de l’information médicale, dans les conditions prévues au deuxième alinéa de l’[article L. 6113-7 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006690710) ;

5° Les traitements effectués par les agences régionales de santé, par l’État et par la personne publique désignée par lui en application du premier alinéa de l’[article L. 6113-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021940593&cidTexte=LEGITEXT000006072665&dateTexte=20180112) du même code, dans le cadre défini au même article.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

