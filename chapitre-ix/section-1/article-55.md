<!-- TITLE: Article 55 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Par dérogation à l’[article 54](/loi-2018/chapitre-ix/article-54), les traitements de données de santé à caractère personnel mis en  œuvre par les organismes ou les services chargés d’une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l’informatique et des libertés, ayant pour seule finalité de répondre, en cas de situation d’urgence, à une alerte sanitaire et d’en gérer les suites, au sens de la [section 1 du chapitre III du titre Ier du livre IV du code de la santé publique](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032404559&cidTexte=LEGITEXT000006072665&dateTexte=20180112), sont soumis aux seules dispositions de la [section 3 du chapitre IV du règlement (UE) 2016/79](/reglement-2016-679/chapitre-iv/section-3).

Les traitements mentionnés au premier alinéa qui utilisent le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques sont mis en œuvre dans les conditions prévues à [l’article 22](/loi-2018/chapitre-iv/article-22).

Les dérogations régies par le premier alinéa du présent article prennent fin un an après la création du traitement s’il continue à être mis en œuvre.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

