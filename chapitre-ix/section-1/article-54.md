<!-- TITLE: Article 54 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

I. – Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.

II. – Des référentiels et règlements types, au sens des [_a bis_ et _b_ du 2° de l’article 11](/loi-2018/chapitre-iii/article-11), s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés en concertation avec l’Institut national des données de santé mentionné à l’[article L. 1462-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) et des organismes publics et privés représentatifs des acteurs concernés.

Les traitements conformes à ces référentiels et règlements types peuvent être mis en œuvre à la condition que leurs responsables adressent préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.

Ces référentiels, peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données de santé présentant un faible risque d’impact sur la vie privée.

III. – Les traitements mentionnés au premier alinéa du I qui ne sont pas conformes à un référentiel ou à un règlement type mentionné au II ne peuvent être mis en œuvre qu’après autorisation par la Commission nationale de l’informatique et des libertés.

L’Institut national des données de santé mentionné à l’article [L. 1462-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) du code de la santé publique peut se saisir ou être saisi, dans des conditions définies par décret en Conseil d’État, par la Commission nationale de l’informatique et des libertés ou le ministre chargé de la santé sur le caractère d’intérêt public que présente le traitement.

IV. – La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

V. – La Commission nationale de l’informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président ou lorsque l’Institut national des données de santé est saisi en application du II du présent article.

Lorsque la commission ne s’est pas prononcée dans ces délais, la demande d’autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en vertu des dispositions du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

