<!-- TITLE: Article 56 -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529402&dateTexte=20040807&categorieLien=id#LEGIARTI000006529402)

----

Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre au responsable d’un traitement de données autorisé en application de l’[article 54](/loi-2018/chapitre-ix/article-54) les données à caractère personnel qu’ils détiennent.

Lorsque ces données permettent l’identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l’informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.

Lorsque le résultat du traitement de données est rendu public, l’identification directe ou indirecte des personnes concernées doit être impossible.

Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à [l’article 226-13 du code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417945).

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

