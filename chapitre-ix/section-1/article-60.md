<!-- TITLE: Article 60 -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529402&dateTexte=20040807&categorieLien=id#LEGIARTI000006529402)

----

Une information relative aux dispositions du présent chapitre doit notamment être assurée dans tout établissement ou centre où s’exercent des activités de prévention, de diagnostic et de soins donnant lieu à la transmission de données à caractère personnel en vue d’un traitement visé au présent chapitre.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

