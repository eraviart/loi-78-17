<!-- TITLE: Article 59 -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529402&dateTexte=20040807&categorieLien=id#LEGIARTI000006529402)

----

Sont destinataires de l’information et exercent les droits de la personne concernée par le traitement les titulaires de l’exercice de l’autorité parentale, pour les mineurs, ou la personne chargée d’une mission de représentation dans le cadre d’une tutelle, d’une habilitation familiale ou d’un mandat de protection future, pour les majeurs protégés dont l’état ne leur permet pas de prendre seul une décision personnelle éclairée.

Par dérogation au premier alinéa du présent article, pour les traitements de données à caractère personnel réalisés dans le cadre de recherches mentionnées aux 2° et 3° de [l’article L. 1121-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665) ou d’études ou d’évaluations dans le domaine de la santé, ayant une finalité d’intérêt public et incluant des personnes mineures, l’information peut être effectuée auprès d’un seul des titulaires de l’exercice de l’autorité parentale, s’il est impossible d’informer l’autre titulaire ou s’il ne peut être consulté dans des délais compatibles avec les exigences méthodologiques propres à la réalisation de la recherche, de l’étude ou de l’évaluation au regard de ses finalités. Le présent alinéa ne fait pas obstacle à l’exercice ultérieur, par chaque titulaire de l’exercice de l’autorité parentale, des droits mentionnés au premier alinéa.

Pour ces traitements, le mineur âgé de quinze ans ou plus peut s’opposer à ce que les titulaires de l’exercice de l’autorité parentale aient accès aux données le concernant recueillies au cours de la recherche, de l’étude ou de l’évaluation. Le mineur reçoit alors l’information et exerce seul ses droits.

Pour ces mêmes traitements, le mineur âgé de quinze ans ou plus peut s’opposer à ce que les titulaires de l’exercice de l’autorité parentale soient informés du traitement de données si le fait d’y participer conduit à révéler une information sur une action de prévention, un dépistage, un diagnostic, un traitement ou une intervention pour laquelle le mineur s’est expressément opposé à la consultation des titulaires de l’autorité parentale en application des articles [L. 1111-5](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685769) et [L. 1111-5-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031917409) du code de la santé publique ou si les liens de famille sont rompus et que le mineur bénéficie à titre personnel du remboursement des prestations en nature de l’assurance maladie et maternité et de la couverture complémentaire mise en place par la [loi n° 99-641 du 27 juillet 1999](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000198392) portant création d’une couverture maladie universelle. Il exerce alors seul ses droits.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

