<!-- TITLE: Article 27 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 34](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205110&dateTexte=20161008&categorieLien=id#LEGIARTI000033205110)
* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 35](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205112&dateTexte=20161008&categorieLien=id#LEGIARTI000033205112)

----

I.-Sont autorisés par décret en Conseil d'Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés :

Les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat, agissant dans l’exercice de ses prérogatives de puissance publique, qui portent sur des données génétiques ou sur des données biométriques nécessaires à l'authentification ou au contrôle de l'identité des personnes.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 7 — DONNÉES SENSIBLES](/etude-impact-490/titre-ier/chapitre-ii/article-7)
  * [ARTICLE 9 — ALLEGEMENT DES FORMALITES PREALABLES](/etude-impact-490/titre-ii/chapitre-ii/article-9)
  * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)
  * [ARTICLES 24](/etude-impact-490/titre-v/articles-24)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)
  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)

<!-- FIN REFERENCES -->

