<!-- TITLE: Article 29 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 4 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529397&dateTexte=20040807&categorieLien=id#LEGIARTI000006529397)

----

Les actes autorisant la création d'un traitement en application des articles 26 et 27 précisent :

1° La dénomination et la finalité du traitement ;

2° Le service auprès duquel s'exerce le droit d'accès défini au chapitre VII ;

3° Les catégories de données à caractère personnel enregistrées ;

4° Les destinataires ou catégories de destinataires habilités à recevoir communication de ces données ;

5° Le cas échéant, les dérogations à l'obligation d'information prévues au V de l'article 32.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

