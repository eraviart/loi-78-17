<!-- TITLE: Article 22 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 34](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205110&dateTexte=20161008&categorieLien=id#LEGIARTI000033205110)

----

Un __ décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés __ détermine les catégories de responsables de traitement et les finalités de ces traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques. La mise en œuvre des traitements intervient sans préjudice des obligations qui incombent aux responsables de traitement ou aux sous-traitants en vertu de la section 3 du chapitre IV du règlement (UE) 2016/679.

Ne sont pas soumis aux dispositions du premier alinéa ceux des traitements portant sur des données à caractère personnel parmi lesquelles figure le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques ou qui requièrent une consultation de ce répertoire :

1° Qui ont exclusivement des finalités de statistique publique, mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au I de l’article 8 ou à l’article 9 ;

2° Qui ont exclusivement des finalités de recherche scientifique ou historique ;

3° Qui mettent à la disposition des usagers de l’administration un ou plusieurs téléservices de l’administration électronique définis à l’article 1er de l’ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, mis en œuvre par l’État ou une personne morale de droit public ou une personne morale de droit privé gérant un service public.

Pour les traitements dont les finalités sont mentionnées aux 1° et 2°, le numéro d’inscription au répertoire national d’identification des personnes physiques fait l’objet préalablement d’une opération cryptographique lui substituant un code statistique non signifiant. Cette opération est renouvelée à une fréquence définie par décret en Conseil d’État pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés. Les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique ne sont pas soumis aux dispositions du premier alinéa.

Pour les traitements dont les finalités sont mentionnées au 1°, l’utilisation du code statistique non signifiant n’est autorisée qu’au sein du service statistique public.

Pour les traitements dont les finalités sont mentionnées au 2°, l’opération cryptographique et, le cas échéant, l’interconnexion de deux fichiers par l’utilisation du code spécifique non signifiant qui en est issu, ne peuvent être assurés par la même personne ni par le responsable de traitement.

À l’exception des traitements mentionnés au second alinéa de l’article 55, le présent article n’est pas applicable aux traitements de données à caractère personnel dans le domaine de la santé qui sont régis par les dispositions du chapitre IX.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [25.](/avis-conseil-etat-393836/delegue-a-la-protection-des-donnees/25)
  * [24.](/avis-conseil-etat-393836/formalites-prealables/24)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 9 — ALLEGEMENT DES FORMALITES PREALABLES](/etude-impact-490/titre-ii/chapitre-ii/article-9)
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)
  * [ARTICLE 22 — MISE A DISPOSITION DE LA LISTE DES TRAITEMENTS AYANT FAIT L’OBJET DE FORMALITES PREALABLES](/etude-impact-490/titre-v/article-22)
  * [ARTICLES 24](/etude-impact-490/titre-v/articles-24)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)
  * [Article 24](/pjl-490/titre-v/article-24)

<!-- FIN REFERENCES -->

