<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Autorisation. -->

* [Article 25](/loi-2018/chapitre-iv/section-2/article-25)
* [Article 26](/loi-2018/chapitre-iv/section-2/article-26)
* [Article 27](/loi-2018/chapitre-iv/section-2/article-27)
* [Article 28](/loi-2018/chapitre-iv/section-2/article-28)
* [Article 29](/loi-2018/chapitre-iv/section-2/article-29)

