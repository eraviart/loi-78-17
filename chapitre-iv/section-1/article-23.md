<!-- TITLE: Article 23 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 4 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529397&dateTexte=20040807&categorieLien=id#LEGIARTI000006529397)

----

I. - La déclaration comporte l'engagement que le traitement satisfait aux exigences de la loi.

Elle peut être adressée à la Commission nationale de l'informatique et des libertés par voie électronique.

La commission délivre sans délai un récépissé, le cas échéant par voie électronique. Le demandeur peut mettre en oeuvre le traitement dès réception de ce récépissé ; il n'est exonéré d'aucune de ses responsabilités.

II. - Les traitements relevant d'un même organisme et ayant des finalités identiques ou liées entre elles peuvent faire l'objet d'une déclaration unique. Dans ce cas, les informations requises en application de l'article 30 ne sont fournies pour chacun des traitements que dans la mesure où elles lui sont propres.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
  * [ARTICLE 9 — ALLEGEMENT DES FORMALITES PREALABLES](/etude-impact-490/titre-ii/chapitre-ii/article-9)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

