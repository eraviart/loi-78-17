<!-- TITLE: Article 43 quater -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [LOI n°2016-1547 du 18 novembre 2016 - art. 91](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033418805&idArticle=LEGIARTI000033423900&dateTexte=20161119&categorieLien=id#LEGIARTI000033423900)

----

La personne concernée peut mandater une association ou une organisation mentionnée au IV de l’article 43 _ter_ aux fins d’exercer en son nom les droits visés aux articles [77](/reglement-2016-679/chapitre-viii/article-77) à [79](/reglement-2016-679/chapitre-viii/article-79) du [règlement (UE) 2016/679](/reglement-2016-679). Elle peut également les mandater pour agir devant la Commission nationale de l’informatique et des libertés, contre celle-ci devant un juge ou contre le responsable du traitement ou le sous-traitant devant une juridiction lorsqu’est en cause un traitement relevant du [chapitre XIII](/loi-2018/chapitre-xiii).

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 16](/pjl-490/titre-ii/chapitre-vi/article-16)

<!-- FIN REFERENCES -->

