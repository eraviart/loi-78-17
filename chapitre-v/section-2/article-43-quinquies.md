<!-- TITLE: Article 43 quinquies -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [LOI n°2016-1547 du 18 novembre 2016 - art. 91](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033418805&idArticle=LEGIARTI000033423900&dateTexte=20161119&categorieLien=id#LEGIARTI000033423900)

----

Dans le cas où, saisie d’une réclamation dirigée contre un responsable de traitement ou un sous-traitant, la Commission nationale de l’informatique et des libertés estime fondés les griefs avancés relatifs à la protection des droits et libertés d’une personne à l’égard du traitement de ses données à caractère personnel, ou de manière générale afin d’assurer la protection de ces droits et libertés dans le cadre de sa mission, elle peut demander au Conseil d’État d’ordonner la suspension ou la cessation du transfert de données en cause, le cas échéant sous astreinte, et assortit alors ses conclusions d’une demande de question préjudicielle à la Cour de justice de l’Union européenne en vue d’apprécier la validité de la décision d’adéquation de la Commission européenne prise sur le fondement de [l’article 45 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-v/article-45) ainsi que de tous les actes pris par la Commission européenne autorisant ou approuvant les garanties appropriées dans le cadre des transferts de données pris sur le fondement de [l’article 46 du même règlement](/reglement-2016-679/chapitre-v/article-46). Lorsque le transfert de données en cause ne constitue pas une opération de traitement effectuée par une juridiction dans l’exercice de sa fonction juridictionnelle, la Commission nationale de l’informatique et des libertés peut saisir dans les mêmes conditions le Conseil d’État pour obtenir la suspension du transfert de données fondé sur une décision d’adéquation de la Commission européenne prise sur le fondement de [l’article 36 de la directive (UE) 2016/680](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32016L0680) dans l’attente de l’appréciation par la Cour de justice de l’Union européenne de la validité de cette décision d’adéquation.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 16](/pjl-490/titre-ii/chapitre-vi/article-16)

<!-- FIN REFERENCES -->

