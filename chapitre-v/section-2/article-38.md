<!-- TITLE: Article 38 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 5 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529398&dateTexte=20040807&categorieLien=id#LEGIARTI000006529398)

----

Toute personne physique a le droit de s'opposer, pour des motifs légitimes, à ce que des données à caractère personnel la concernant fassent l'objet d'un traitement.

Elle a le droit de s'opposer, sans frais, à ce que les données la concernant soient utilisées à des fins de prospection, notamment commerciale, par le responsable actuel du traitement ou celui d'un traitement ultérieur.

Les dispositions du premier alinéa ne s'appliquent pas lorsque le traitement répond à une obligation légale ou lorsque l'application de ces dispositions a été écartée par une disposition expresse de l'acte autorisant le traitement.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 15 — LIMITATION DES DROITS](/etude-impact-490/titre-ii/chapitre-iv/article-15)

<!-- FIN REFERENCES -->

