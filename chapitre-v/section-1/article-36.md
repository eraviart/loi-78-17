<!-- TITLE: Article 36 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 62](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205080&dateTexte=20161008&categorieLien=id#LEGIARTI000033205080)

----

Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques ; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article [L. 212-3 du code du patrimoine](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845568&dateTexte=&categorieLien=cid).

Il peut être procédé à un traitement ayant des finalités autres que celles mentionnées au premier alinéa :

-soit avec l'accord exprès de la personne concernée ou en vertu de ses directives, formulées dans les conditions définies à l'article 40-1 ;

-soit dans les conditions prévues au 8° du II et au IV de l'article 8 s'agissant de données mentionnées au I de ce même article.

Lorsque les traitements de données à caractère personnel sont mis en œuvre par les services publics d’archives à des fins archivistiques dans l’intérêt public conformément à [l’article L. 211-2 du code du patrimoine](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180112), les droits visés aux articles [15](/reglement-2016-679/chapitre-iii/section-2/article-15), [16](reglement-2016-679/chapitre-iii/section-2/article-16), [18](reglement-2016-679/chapitre-iii/section-2/article-18), [19](reglement-2016-679/chapitre-iii/section-2/article-19), [20](reglement-2016-679/chapitre-iii/section-2/article-20) et [21](reglement-2016-679/chapitre-iii/section-2/article-21) du [règlement (UE) 2016/679](reglement-2016-679/) ne s’appliquent pas dans la mesure où ces droits rendent impossible ou entravent sérieusement la réalisation des finalités spécifiques et où de telles dérogations sont nécessaires pour atteindre ces finalités. Les conditions et garanties appropriées prévues à [l’article 89 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ix/article-89) sont déterminées par le code du patrimoine et les autres dispositions législatives et réglementaires applicables aux archives publiques. Elles sont également assurées par le respect des normes conformes à l’état de l’art en matière d’archivage électronique.
<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [28.](/avis-conseil-etat-393836/donnees-dinfraction/28)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 12 — TRAITEMENTS ARCHIVISTIQUES](/etude-impact-490/titre-ii/chapitre-iv/article-12)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 12](/pjl-490/titre-ii/chapitre-iv/article-12)
  * [Article 19](/pjl-490/titre-iii/article-19)
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

