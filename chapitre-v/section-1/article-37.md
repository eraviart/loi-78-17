<!-- TITLE: Article 37 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [ORDONNANCE n° 2015-1341 du 23 octobre 2015 - art. 3 (V)](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031360943&idArticle=LEGIARTI000031365580&dateTexte=20151025&categorieLien=id#LEGIARTI000031365580)

----

Les dispositions de la présente loi ne font pas obstacle à l'application, au bénéfice de tiers, des dispositions du [livre III du code des relations entre le public et l'administration](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367687&dateTexte=&categorieLien=cid) et des dispositions du livre II du code du patrimoine.

En conséquence, ne peut être regardé comme un tiers non autorisé au sens de l'article 34 le titulaire d'un droit d'accès aux documents administratifs ou aux archives publiques exercé conformément au livre III du code des relations entre le public et l'administration et au livre II du code du patrimoine.

