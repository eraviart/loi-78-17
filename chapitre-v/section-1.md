<!-- TITLE: Section 1 -->
<!-- SUBTITLE: Obligations incombant aux responsables de traitements. -->

* [Article 32](/loi-2018/chapitre-v/section-1/article-32)
* [Article 33](/loi-2018/chapitre-v/section-1/article-33)
* [Article 34](/loi-2018/chapitre-v/section-1/article-34)
* [Article 34 bis](/loi-2018/chapitre-v/section-1/article-34-bis)
* [Article 35](/loi-2018/chapitre-v/section-1/article-35)
* [Article 36](/loi-2018/chapitre-v/section-1/article-36)
* [Article 37](/loi-2018/chapitre-v/section-1/article-37)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)

<!-- FIN REFERENCES -->

