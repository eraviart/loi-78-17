<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Droits des personnes à l'égard des traitements de données à caractère personnel. -->

* [Article 38](/loi-2018/chapitre-v/section-2/article-38)
* [Article 39](/loi-2018/chapitre-v/section-2/article-39)
* [Article 40](/loi-2018/chapitre-v/section-2/article-40)
* [Article 40-1](/loi-2018/chapitre-v/section-2/article-40-1)
* [Article 41](/loi-2018/chapitre-v/section-2/article-41)
* [Article 42](/loi-2018/chapitre-v/section-2/article-42)
* [Article 43](/loi-2018/chapitre-v/section-2/article-43)
* [Article 43 bis](/loi-2018/chapitre-v/section-2/article-43-bis)
* [Article 43 ter](/loi-2018/chapitre-v/section-2/article-43-ter)
* [Article 43 quater](/loi-2018/chapitre-v/section-2/article-43-quater)
* [Article 43 quinquies](/loi-2018/chapitre-v/section-2/article-43-quinquies)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [37.](/avis-conseil-etat-393836/transfert-de-donnees-personnelles-vers-un-pays-tiers/37)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 15 — LIMITATION DES DROITS](/etude-impact-490/titre-ii/chapitre-iv/article-15)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 17](/pjl-490/titre-ii/chapitre-vi/article-17)

<!-- FIN REFERENCES -->

