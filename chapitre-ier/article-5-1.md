<!-- TITLE: Article 5-1 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 1 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529394&dateTexte=20040807&categorieLien=id#LEGIARTI000006529394)

----

Les règles nationales, prises sur le fondement des dispositions du règlement (UE) 2016/679 renvoyant au droit national le soin d’adapter ou de compléter les droits et obligations prévus par ce règlement, s’appliquent dès lors que la personne concernée réside en France, y compris lorsque le responsable de traitement n’est pas établi en France.

Toutefois, lorsqu’est en cause un des traitements mentionnés au 2 de l’article 85 du même règlement, les règles nationales mentionnées au premier alinéa sont celles dont relève le responsable de traitement, lorsqu’il est établi dans l’Union européenne.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 8 — CRITERE D’APPLICATION DU DROIT](/etude-impact-490/titre-ii/chapitre-ier/article-8)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 8](/pjl-490/titre-ii/chapitre-i/article-8)

<!-- FIN REFERENCES -->

