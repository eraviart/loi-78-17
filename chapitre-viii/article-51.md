<!-- TITLE: Article 51 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 7](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782614&dateTexte=20110330&categorieLien=id#LEGIARTI000023782614)

----

Est puni d'un an d'emprisonnement et de 15 000 euros d'amende le fait d'entraver l'action de la Commission nationale de l'informatique et des libertés :

1° Soit en s'opposant à l'exercice des missions confiées à ses membres ou aux agents habilités en application du dernier alinéa de l'article 19 lorsque la visite a été autorisée par le juge ;

2° Soit en refusant de communiquer à ses membres ou aux agents habilités en application du dernier alinéa de l'article 19 les renseignements et documents utiles à leur mission, ou en dissimulant lesdits documents ou renseignements, ou en les faisant disparaître ;

3° Soit en communiquant des informations qui ne sont pas conformes au contenu des enregistrements tel qu'il était au moment où la demande a été formulée ou qui ne présentent pas ce contenu sous une forme directement accessible.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 4 — POUVOIRS DE CONTRÔLE DE LA CNIL](/etude-impact-490/titre-ier/chapitre-ier/article-4)

<!-- FIN REFERENCES -->

