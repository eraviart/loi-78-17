<!-- TITLE: Article 50 -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [Loi n°2004-801 du 6 août 2004 - art. 8 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529401&dateTexte=20040807&categorieLien=id#LEGIARTI000006529401)

----

Les infractions aux dispositions de la présente loi sont prévues et réprimées par les [articles 226-16 à 226-24](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417955&dateTexte=&categorieLien=cid) du code pénal.

