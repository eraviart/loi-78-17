<!-- TITLE: Article 65 (abrogé) -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [Loi n°2004-801 du 6 août 2004 - art. 10 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529403&dateTexte=20040807&categorieLien=id#LEGIARTI000006529403)
* Abrogé par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

