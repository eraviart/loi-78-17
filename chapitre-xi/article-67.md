<!-- TITLE: Article 67 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 63](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205208&dateTexte=20161008&categorieLien=id#LEGIARTI000033205208)

----

Le 5° de l'article 6, les articles 8, 9, 32, et 39, le I de l'article 40 et les articles 68 à 70 ne s'appliquent pas aux traitements de données à caractère personnel mis en oeuvre aux seules fins :

1° D'expression littéraire et artistique ;

2° D'exercice, à titre professionnel, de l'activité de journaliste, dans le respect des règles déontologiques de cette profession.

En cas de non-respect des dispositions de la loi applicables aux traitements prévus par le présent article, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de se mettre en conformité avec la loi.

Les dispositions des alinéas précédents ne font pas obstacle à l'application des dispositions du code civil, des lois relatives à la presse écrite ou audiovisuelle et du code pénal, qui prévoient les conditions d'exercice du droit de réponse et qui préviennent, limitent, réparent et, le cas échéant, répriment les atteintes à la vie privée et à la réputation des personnes.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

