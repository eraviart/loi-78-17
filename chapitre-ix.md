<!-- TITLE: Chapitre IX -->
<!-- SUBTITLE: Traitements de données à caractère personnel dans le domaine de la santé. -->

* [Section 1 — Dispositions générales.](/loi-2018/chapitre-ix/section-1)

  * [Article 53](/loi-2018/chapitre-ix/section-1/article-53)
  * [Article 54](/loi-2018/chapitre-ix/section-1/article-54)
  * [Article 55](/loi-2018/chapitre-ix/section-1/article-55)
  * [Article 56](/loi-2018/chapitre-ix/section-1/article-56)
  * [Article 57](/loi-2018/chapitre-ix/section-1/article-57)
  * [Article 58](/loi-2018/chapitre-ix/section-1/article-58)
  * [Article 59](/loi-2018/chapitre-ix/section-1/article-59)
  * [Article 60](/loi-2018/chapitre-ix/section-1/article-60)

* [Section 2 —  Dispositions particulières aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé.](/loi-2018/chapitre-ix/section-2)

  * [Article 61](/loi-2018/chapitre-ix/section-2/article-61)
  * [Article 62](/loi-2018/chapitre-ix/section-2/article-62)
  * [Article 63](/loi-2018/chapitre-ix/section-2/article-63)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [23.](/avis-conseil-etat-393836/formalites-prealables/23)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 1<sup>er</sup>](/pjl-490/titre-i/chapitre-i/article-1)
  * [Article 7](/pjl-490/titre-i/chapitre-ii/article-7)
  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)

<!-- FIN REFERENCES -->
