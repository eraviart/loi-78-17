<!-- TITLE: Chapitre XIII -->
<!-- SUBTITLE: Dispositions applicables aux traitements relevant de la directive (UE) 2016/680 du 27 avril 2016. -->

* [Section 1 - Dispositions générales.](/loi-2018/chapitre-xiii/section-1)

  * [Article 70-1](/loi-2018/chapitre-xiii/section-1/article-70-1)
  * [Article 70-2](/loi-2018/chapitre-xiii/section-1/article-70-2)
  * [Article 70-3](/loi-2018/chapitre-xiii/section-1/article-70-3)
  * [Article 70-4](/loi-2018/chapitre-xiii/section-1/article-70-4)
  * [Article 70-5](/loi-2018/chapitre-xiii/section-1/article-70-5)
  * [Article 70-6](/loi-2018/chapitre-xiii/section-1/article-70-6)
  * [Article 70-7](/loi-2018/chapitre-xiii/section-1/article-70-7)
  * [Article 70-8](/loi-2018/chapitre-xiii/section-1/article-70-8)
  * [Article 70-9](/loi-2018/chapitre-xiii/section-1/article-70-9)
  * [Article 70-10](/loi-2018/chapitre-xiii/section-1/article-70-10)

* [Section 2 — Obligations incombant aux autorités compétentes et aux responsables de traitements.](/loi-2018/chapitre-xiii/section-2)

  * [Article 70-11](/loi-2018/chapitre-xiii/section-2/article-70-11)
  * [Article 70-12](/loi-2018/chapitre-xiii/section-2/article-70-12)
  * [Article 70-13](/loi-2018/chapitre-xiii/section-2/article-70-13)
  * [Article 70-14](/loi-2018/chapitre-xiii/section-2/article-70-14)
  * [Article 70-15](/loi-2018/chapitre-xiii/section-2/article-70-15)
  * [Article 70-16](/loi-2018/chapitre-xiii/section-2/article-70-16)
  * [Article 70-17](/loi-2018/chapitre-xiii/section-2/article-70-17)

* [Section 3 — Droits de la personne concernée.](/loi-2018/chapitre-xiii/section-3)

  * [Article 70-18](/loi-2018/chapitre-xiii/section-3/article-70-18)
  * [Article 70-19](/loi-2018/chapitre-xiii/section-3/article-70-19)
  * [Article 70-20](/loi-2018/chapitre-xiii/section-3/article-70-20)
  * [Article 70-21](/loi-2018/chapitre-xiii/section-3/article-70-21)
  * [Article 70-22](/loi-2018/chapitre-xiii/section-3/article-70-22)
  * [Article 70-23](/loi-2018/chapitre-xiii/section-3/article-70-23)
  * [Article 70-24](/loi-2018/chapitre-xiii/section-3/article-70-24)

* [Section 4 — Transferts de données à caractère personnel vers des États n’appartenant pas à l’Union européenne ou vers des destinataires établis dans des États non membres de l’Union européenne.](/loi-2018/chapitre-xiii/section-4)

  * [Article 70-25](/loi-2018/chapitre-xiii/section-4/article-70-25)
  * [Article 70-26](/loi-2018/chapitre-xiii/section-4/article-70-26)
  * [Article 70-27](/loi-2018/chapitre-xiii/section-4/article-70-27)