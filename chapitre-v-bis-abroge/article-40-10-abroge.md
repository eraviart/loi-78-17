<!-- TITLE: Article 40-10 (abrogé) -->

[En savoir plus...](article.url_en_savoir_plus)

* Abrogé par [Loi 2004-801 2004-08-07 art. 9 III JORF 7 août 2004](None)
* Créé par [Loi n°94-548 du 1 juillet 1994 - art. 1 JORF 2 juillet 1994](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000547135&idArticle=LEGIARTI000006527877&dateTexte=19940702&categorieLien=id#LEGIARTI000006527877)

----

