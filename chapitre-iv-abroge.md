<!-- TITLE: Chapitre IV -->
<!-- SUBTITLE: Collecte, enregistrement et conservation des informations nominatives. (abrogé) -->

* [Article 29-1 (abrogé)](/loi-2018/chapitre-iv-abroge/article-29-1-abroge)
* [Article 33-1 (abrogé)](/loi-2018/chapitre-iv-abroge/article-33-1-abroge)

