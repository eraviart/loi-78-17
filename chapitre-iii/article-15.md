<!-- TITLE: Article 15 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Sous réserve des compétences du bureau et de la formation restreinte, la commission se réunit en formation plénière.

En cas de partage égal des voix, la voix du président est prépondérante.

La commission peut charger le président ou le vice-président délégué d'exercer celles de ses attributions mentionnées :

\- aux e et f du 2° de l'article 11 ;

\- au c du 2° de l'article 11 ;

\- au d du 4° de l'article 11 ;

\- aux articles 41 et 42 ;

\- à l'article 54 ;

\- aux deux derniers alinéas de l'article 69, à l'exception des traitements mentionnés aux I ou II de l'article 26 ;

\- au premier alinéa de l'article 70.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

