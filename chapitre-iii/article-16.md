<!-- TITLE: Article 16 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 6](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782612&dateTexte=20110330&categorieLien=id#LEGIARTI000023782612)

----

Le bureau peut être chargé par la commission d'exercer les attributions de celle-ci mentionnées :

\- au dernier alinéa de l'article 19 ;

\- à l'article 25, en cas d'urgence ;

\- au second alinéa de l'article 70.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 3](/pjl-490/titre-i/chapitre-i/article-3)
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

