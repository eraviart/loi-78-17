<!-- TITLE: Article 15 bis -->

[En savoir plus...](article.url_en_savoir_plus)

* Créé par [LOI n°2016-1321 du 7 octobre 2016 - art. 26](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205094&dateTexte=20161008&categorieLien=id#LEGIARTI000033205094)

----

La Commission nationale de l'informatique et des libertés et la Commission d'accès aux documents administratifs se réunissent dans un collège unique, sur l'initiative conjointe de leurs présidents, lorsqu'un sujet d'intérêt commun le justifie.

