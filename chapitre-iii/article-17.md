<!-- TITLE: Article 17 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 3](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782608&dateTexte=20110330&categorieLien=id#LEGIARTI000023782608)

----

La formation restreinte <mark style="background-color: #F8EDA8;">prend les mesures et</mark> prononce les sanctions à l'encontre des responsables de traitements qui ne respectent pas les obligations découlant du <mark style="background-color: #F8EDA8;">[règlement (UE) 2016/679](/reglement-2016-679)</mark> de la présente loi dans les conditions prévues au chapitre VII.

<mark style="background-color: #F8EDA8;">  Les membres délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance.</mark>

Les membres de la formation restreinte ne peuvent participer à l'exercice des attributions de la commission mentionnées aux c, e et f du 2° de l'article 11 et à l'article 44.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [13.](/avis-conseil-etat-393836/commissaire-du-gouvernement-et-formation-restreinte/13)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 3](/pjl-490/titre-i/chapitre-i/article-3)

<!-- FIN REFERENCES -->

