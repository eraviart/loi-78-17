<!-- TITLE: Article 18 -->

[En savoir plus...](article.url_en_savoir_plus)

* Modifié par [Loi n°2004-801 du 6 août 2004 - art. 3 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529396&dateTexte=20040807&categorieLien=id#LEGIARTI000006529396)

----

Un commissaire du Gouvernement, désigné par le Premier ministre, siège auprès de la commission. Des commissaires adjoints peuvent être désignés dans les mêmes conditions.

~~Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière ou en formation restreinte, ainsi qu'à celles des réunions de son bureau qui ont pour objet l'exercice des attributions déléguées en vertu de l'article 16 ; il est rendu destinataire de tous ses avis et décisions.~~ <mark style="background-color: #F8EDA8;"> Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière, ainsi qu’à celles des réunions de son bureau qui ont pour objet l’exercice des attributions déléguées en vertu de l’article 16. Il peut assister aux séances de la formation restreinte, sans être présent au délibéré. Il est rendu destinataire de l’ensemble des avis et décisions de la commission et de la formation restreinte.</mark>

~~Il peut, sauf en matière de sanctions, provoquer une seconde délibération, qui doit intervenir dans les dix jours de la délibération initiale.~~ <mark style="background-color: #F8EDA8;">  Sauf en matière de mesures ou de sanctions relevant du chapitre VII, il peut provoquer une seconde délibération de la commission, qui doit intervenir dans les dix jours de la délibération initiale.</mark>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [13.](/avis-conseil-etat-393836/commissaire-du-gouvernement-et-formation-restreinte/13)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 2 et 3 — COMMISSAIRE DU GOUVERNEMENT ET MEMBRES DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 3](/pjl-490/titre-i/chapitre-i/article-3)

<!-- FIN REFERENCES -->

