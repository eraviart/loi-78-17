<!-- TITLE: Section 1 -->
<!-- SUBTITLE: Dispositions générales. -->

* [Article 70-1](/loi-2018/chapitre-xiii/section-1/article-70-1)
* [Article 70-2](/loi-2018/chapitre-xiii/section-1/article-70-2)
* [Article 70-3](/loi-2018/chapitre-xiii/section-1/article-70-3)
* [Article 70-4](/loi-2018/chapitre-xiii/section-1/article-70-4)
* [Article 70-5](/loi-2018/chapitre-xiii/section-1/article-70-5)
* [Article 70-6](/loi-2018/chapitre-xiii/section-1/article-70-6)
* [Article 70-7](/loi-2018/chapitre-xiii/section-1/article-70-7)
* [Article 70-8](/loi-2018/chapitre-xiii/section-1/article-70-8)
* [Article 70-9](/loi-2018/chapitre-xiii/section-1/article-70-9)
* [Article 70-10](/loi-2018/chapitre-xiii/section-1/article-70-10)