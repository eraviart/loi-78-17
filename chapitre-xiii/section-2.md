<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Obligations incombant aux autorités compétentes et aux responsables de traitements. -->

* [Article 70-11](/loi-2018/chapitre-xiii/section-2/article-70-11)
* [Article 70-12](/loi-2018/chapitre-xiii/section-2/article-70-12)
* [Article 70-13](/loi-2018/chapitre-xiii/section-2/article-70-13)
* [Article 70-14](/loi-2018/chapitre-xiii/section-2/article-70-14)
* [Article 70-15](/loi-2018/chapitre-xiii/section-2/article-70-15)
* [Article 70-16](/loi-2018/chapitre-xiii/section-2/article-70-16)
* [Article 70-17](/loi-2018/chapitre-xiii/section-2/article-70-17)