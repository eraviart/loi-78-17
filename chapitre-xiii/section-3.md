<!-- TITLE: Section 3 -->
<!-- SUBTITLE: Droits de la personne concernée. -->

* [Article 70-18](/loi-2018/chapitre-xiii/section-3/article-70-18)
* [Article 70-19](/loi-2018/chapitre-xiii/section-3/article-70-19)
* [Article 70-20](/loi-2018/chapitre-xiii/section-3/article-70-20)
* [Article 70-21](/loi-2018/chapitre-xiii/section-3/article-70-21)
* [Article 70-22](/loi-2018/chapitre-xiii/section-3/article-70-22)
* [Article 70-23](/loi-2018/chapitre-xiii/section-3/article-70-23)
* [Article 70-24](/loi-2018/chapitre-xiii/section-3/article-70-24)