<!-- TITLE: Article 70-26 -->


Par dérogation aux dispositions de l’article précédent, le responsable d’un traitement de données à caractère personnel ne peut, en l’absence de décision d’adéquation ou de garanties appropriées, transférer ces données ou autoriser le transfert de données déjà transmises vers un État n’appartenant pas à l’Union européenne que lorsque le transfert est nécessaire :

1° À la sauvegarde des intérêts vitaux de la personne concernée ou d’une autre personne ;

2° À la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit français le prévoit ;

3° Pour prévenir une menace grave et immédiate pour la sécurité publique d’un État membre de l’Union européenne ou d’un pays tiers ;

4° Dans des cas particuliers, à l’une des finalités énoncées au 1° de l’article 70-1 ;

5° Dans un cas particulier, à la constatation, à l’exercice ou à la défense de droits en justice en rapport avec les mêmes fins.

Dans les cas visés aux 4° et 5°, le responsable du traitement de données à caractère personnel ne transfère pas ces données s’il estime que les libertés et droits fondamentaux de la personne concernée l’emportent sur l’intérêt public dans le cadre du transfert envisagé.

Lorsqu’un transfert est effectué aux fins de la sauvegarde des intérêts légitimes de la personne concernée, le responsable du traitement garde trace de la date et l’heure du transfert, des informations sur l’autorité compétente destinataire, et de la justification du transfert et les données à caractère personnel transférées. Il met ces informations à la disposition de la Commission nationale de l’informatique et des libertés, à sa demande.