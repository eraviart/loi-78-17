<!-- TITLE: Article 70-19 -->


La personne concernée a le droit d’obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu’elles le sont, l’accès auxdites données ainsi que les informations suivantes :

1° Les finalités du traitement ainsi que sa base juridique ;

2° Les catégories de données à caractère personnel concernées ;

3° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des États non membres de l’Union européenne ou les organisations internationales ;

4° Lorsque cela est possible, la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n’est pas possible, les critères utilisés pour déterminer cette durée ;

5° L’existence du droit de demander au responsable du traitement la rectification ou l’effacement des données à caractère personnel, ou la limitation du traitement de ces données ;

6° Le droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

7° La communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.