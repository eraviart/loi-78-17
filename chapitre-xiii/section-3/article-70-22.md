<!-- TITLE: Article 70-22 -->


En cas de restriction des droits de la personne concernée intervenue en application du II ou du III de l’article 70-21, la personne concernée peut saisir la Commission nationale de l’informatique et des libertés.

Les dispositions des deuxième et troisième alinéas de l’[article 41](/loi-2018/chapitre-v/section-2/article-41) sont alors applicables.

Lorsque la commission informe la personne concernée qu’il a été procédé aux vérifications nécessaires, elle l’informe également de son droit de former un recours juridictionnel.