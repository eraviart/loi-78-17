<!-- TITLE: Article 70-23 -->


Aucun paiement n’est exigé pour prendre les mesures et fournir les informations visées aux articles 70-18 à 70-20, sauf en cas de demande manifestement infondée ou abusive.

Dans ce cas, le responsable du traitement peut également refuser de donner suite à la demande.

En cas de contestation, la charge de la preuve du caractère manifestement infondé ou abusif des demandes incombe au responsable du traitement auprès duquel elles sont adressées.