<!-- TITLE: Article 70-24 -->


Les dispositions de la présente sous-section ne s’appliquent pas lorsque les données à caractère personnel figurent soit dans une décision judiciaire, soit dans un dossier judiciaire faisant l’objet d’un traitement lors d’une procédure pénale. Dans ces cas, l’accès à ces données ne peut se faire que dans les conditions prévues par le code de procédure pénale.