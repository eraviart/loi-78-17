<!-- TITLE: Article 70-20 -->


I. – La personne concernée a le droit d’obtenir du responsable du traitement :

1° Que soit rectifiées dans les meilleurs délais des données à caractère personnel la concernant qui sont inexactes ;

2° Que soient complétées des données à caractère personnel la concernant incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;

3° Que soit effacées dans les meilleurs délais des données à caractère personnel la concernant lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

II. – Lorsque l’intéressé en fait la demande, le responsable du traitement doit justifier qu’il a procédé aux opérations exigées en vertu du I.

III. – Au lieu de procéder à l’effacement, le responsable du traitement limite le traitement lorsque :

1° Soit l’exactitude des données à caractère personnel est contestée par la personne concernée et il ne peut être déterminé si les données sont exactes ou non ;

2° Soit les données à caractère personnel doivent être conservées à des fins probatoires.

Lorsque le traitement est limité en vertu du 1°, le responsable du traitement informe la personne concernée avant de lever la limitation du traitement.

IV. – Le responsable du traitement informe la personne concernée de tout refus de rectifier ou d’effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus.

V. – Le responsable du traitement communique la rectification des données à caractère personnel inexactes à l’autorité compétente dont elles proviennent.

VI. – Lorsque des données à caractère personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des I, II et III, le responsable du traitement le notifie aux destinataires afin que ceux-ci rectifient ou effacent les données ou limitent le traitement des données sous leur responsabilité.