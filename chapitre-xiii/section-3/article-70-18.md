<!-- TITLE: Article 70-18 -->


I. – Le responsable du traitement met à la disposition de la personne concernée les informations suivantes :

1° L’identité et les coordonnées du responsable du traitement, et le cas échéant celles de son représentant ;

2° Le cas échéant, les coordonnées du délégué à la protection des données ;

3° Les finalités poursuivies par le traitement auquel les données sont destinées ;

4° Le droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

5° L’existence du droit de demander au responsable du traitement l’accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.

II. – En plus des informations visées au I, le responsable du traitement fournit à la personne concernée, dans des cas particuliers, les informations additionnelles suivantes afin de lui permettre d’exercer ses droits :

1° La base juridique du traitement ;

2° La durée de conservation des données à caractère personnel ou, lorsque ce n’est pas possible, les critères utilisés pour déterminer cette durée ;

3° Le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les États non membres de l’Union européenne ou au sein d’organisations internationales ;

4° Au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l’insu de la personne concernée.