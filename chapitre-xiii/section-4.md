<!-- TITLE: Section 4 -->
<!-- SUBTITLE: Transferts de données à caractère personnel vers des États n’appartenant pas à l’Union européenne ou vers des destinataires établis dans des États non membres de l’Union européenne. -->

* [Article 70-25](/loi-2018/chapitre-xiii/section-4/article-70-25)
* [Article 70-26](/loi-2018/chapitre-xiii/section-4/article-70-26)
* [Article 70-27](/loi-2018/chapitre-xiii/section-4/article-70-27)