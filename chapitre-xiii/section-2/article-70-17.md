<!-- TITLE: Article 70-17 -->


I. – Sauf pour les juridictions agissant dans l’exercice de leur fonction juridictionnelle, le responsable du traitement désigne un délégué à la protection des données.

Un seul délégué à la protection des données peut être désigné pour plusieurs autorités compétentes, compte tenu de leur structure organisationnelle et de leur taille.

Les dispositions des [paragraphes 5 et 7 de l’article 37](/reglement-2016-679/chapitre-iv/section-4/article-37), des [paragraphes 1 et 2 de l’article 38](/reglement-2016-679/chapitre-iv/section-4/article-38) et du [paragraphe 1 de l’article 39](/reglement-2016-679/chapitre-iv/section-4/article-39) du règlement (UE) 2016/679, en ce qu’elles concernent le responsable du traitement, sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.