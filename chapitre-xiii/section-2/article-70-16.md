<!-- TITLE: Article 70-16 -->


Les articles [31](/reglement-2016-679/chapitre-iv/section-1/article-31), [33](/reglement-2016-679/chapitre-iv/section-2/article-33) et [34](/reglement-2016-679/chapitre-iv/section-2/article-34) du règlement (UE) 2016/679 sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

Si la violation de données à caractère personnel porte sur des données à caractère personnel qui ont été transmises par le responsable du traitement d’un autre État membre ou à celui-ci, le responsable du traitement notifie également la violation au responsable du traitement de l’autre État membre dans les meilleurs délais.

La communication d’une violation de données à caractère personnel à la personne concernée peut être retardée, limitée ou ne pas être délivrée, dès lors et aussi longtemps qu’une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée, lorsque sa mise en œuvre est de nature à mettre en danger la sécurité publique, la sécurité nationale ou les droits ou libertés d’autrui ou à faire obstacle au bon déroulement des enquêtes et procédures destinées à prévenir, détecter ou poursuivre des infractions pénales ou à exécuter des sanctions pénales.