<!-- TITLE: Article 70-13 -->


I. – Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux [paragraphes 1 et 2 de l’article 24](/reglement-2016-679/chapitre-iv/section-1/article-24) et aux [paragraphes 1 et 2 de l’article 25](/reglement-2016-679/chapitre-iv/section-1/article-25) du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l’[article 8](/loi-2018/chapitre-ii/section-2/article-8).

II. – En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d’une évaluation des risques, des mesures destinées à :

1° Empêcher toute personne non autorisée d’accéder aux installations utilisées pour le traitement (contrôle de l’accès aux installations) ;

2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;

3° Empêcher l’introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l’inspection, la modification ou l’effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;

4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l’aide d’installations de transmission de données (contrôle des utilisateurs) ;

5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu’aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l’accès aux données) ;

6° Garantir qu’il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;

7° Garantir qu’il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l’introduction) ;

8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;

9° Garantir que les systèmes installés puissent être rétablis en cas d’interruption (restauration) ;

10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).