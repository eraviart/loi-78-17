<!-- TITLE: Article 70-4 -->


Si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données mentionnées au [I de l’article 8](/loi-2018/chapitre-ii/section-2/article-8), le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel.

Si le traitement est mis en œuvre pour le compte de l’État, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par [l’article 30](/loi-2018/chapitre-iv/section-3/article-30).

Dans les autres cas, le responsable du traitement ou le sous-traitant consulte la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :

1° Soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;

2° Soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.