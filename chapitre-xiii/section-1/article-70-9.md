<!-- TITLE: Article 70-9 -->


Aucune décision de justice impliquant une appréciation sur le comportement d’une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Aucune autre décision produisant des effets juridiques à l’égard d’une personne ne peut être prise sur le seul fondement d’un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée.

Tout profilage qui entraîne une discrimination à l’égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l’[article 8](/loi-2018/chapitre-ii/section-2/article-8) est interdit.