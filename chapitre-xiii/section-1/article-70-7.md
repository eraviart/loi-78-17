<!-- TITLE: Article 70-7 -->


Les traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques sont mis en œuvre dans les conditions de l’[article 36](/loi-2018/chapitre-v/section-1/article-36) de la présente loi.
