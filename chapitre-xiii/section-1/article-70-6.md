<!-- TITLE: Article 70-6 -->


Les traitements effectués pour l’une des finalités énoncées au 1° de l’article 70-1 autre que celles pour lesquelles les données ont été collectées sont autorisés sous réserve du respect des principes prévus au chapitre Ier de la présente loi et au présent chapitre.

Ces traitements peuvent comprendre l’archivage dans l’intérêt public, à des fins scientifiques, statistiques ou historiques, aux fins énoncées à l’article 70-1.