<!-- TITLE: Article 70-10 -->


Les données à caractère personnel ne peuvent faire l’objet d’une opération de traitement de la part d’un sous-traitant que dans les conditions prévues aux [paragraphes 1, 2, 9 et 10 de l’article 28](/reglement-2016-679/chapitre-iv/section-1/article-28) et à l’[article 29](/reglement-2016-679/chapitre-iv/section-1/article-29) du règlement (UE) 2016/679 et au présent article.

Les sous-traitants doivent présenter des garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière que le traitement réponde aux exigences du présent chapitre et garantisse la protection des droits de la personne concernée.

Le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l’égard du responsable du traitement, définit l’objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement, et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement. Le contenu de ce contrat ou acte juridique est précisé par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés.