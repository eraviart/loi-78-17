<!-- TITLE: Article 70-1 -->


Les dispositions du présent chapitre s’appliquent, le cas échéant par dérogation aux autres dispositions de la présente loi, aux traitements des données à caractère personnel mis en œuvre :

1° À des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces ;

2° Par toute autorité publique compétente pour l’une des finalités énoncées au 1°, ou tout autre organisme ou entité à qui a été confié, à ces mêmes fins, l’exercice de l’autorité publique et des prérogatives de puissance publique, ci-après dénommée autorité compétente.

Ces traitements ne sont licites que si et dans la mesure où ils sont nécessaires à l’exécution d’une mission effectuée, pour les finalités énoncées au 1°, par une autorité compétente au sens du 2°, et où sont respectées les dispositions des articles 70-3 et 70-4.

Pour l’application du présent chapitre, lorsque les notions utilisées ne sont pas définies au chapitre premier de la présente loi, les définitions de [l’article 4 du règlement (UE) 2016/679](/reglement-2016-679) sont applicables.
