<!-- TITLE: Article 70-2 -->


Le traitement de données mentionnées au [I de l’article 8](/loi-2018/chapitre-ii/section-2/article-8) est possible uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et, soit s’il est prévu par un acte législatif ou règlementaire, soit s’il vise à protéger les intérêts vitaux d’une personne physique, soit s’il porte sur des données manifestement rendues publiques par la personne concernée.
