<!-- TITLE: Article 70-3 -->


Si le traitement est mis en œuvre pour le compte de l’État pour au moins l’une des finalités prévues au 1° de l’article 70-1, il doit être prévu par un acte règlementaire pris conformément au [I de l’article 26](/loi-2018/chapitre-iv/section-2/article-26) et aux articles [28](/loi-2018/chapitre-iv/section-2/article-28) à [31](/loi-2018/chapitre-iv/section-3/article-31).

Si le traitement porte sur des données mentionnées au [I de l’article 8](/loi-2018/chapitre-ii/section-2/article-8), il est prévu par un acte règlementaire pris conformément au [II de l’article 26](/loi-2018/chapitre-iv/section-2/article-26).
