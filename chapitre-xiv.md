<!-- TITLE: Chapitre XIV -->
<!-- SUBTITLE: Dispositions diverses. -->

* [Article 71](/loi-2018/chapitre-xiv/article-71)
* [Article 72](/loi-2018/chapitre-xiv/article-72)
